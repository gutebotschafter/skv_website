<?php

define('DS', DIRECTORY_SEPARATOR);

/**
 * Checks if the file exists and put the content into the returning string
 *
 * @param string $file The file to get the inline content
 * @param string $tag The tag for inline content
 * @param bool $development Show in development mode
 * @return string Returns the string if there is content or false if there is nothing
 */
function inlineRenderer($file, $tag = "style", $development = false) {
    $path = __DIR__ . DS . '..' . DS . '..' . DS . '..' . DS . 'public' . $file;
    
    if (file_exists($path) && $development) {
        $content = file_get_contents($path);

        return "<" . $tag . ">" . $content . "</" . $tag . ">";
    }

    return "";
}
