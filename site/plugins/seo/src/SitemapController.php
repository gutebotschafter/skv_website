<?php

namespace GutebBotschafter\Seo;

use Kirby\Http\Response;

/**
 * Class MailController
 * @package GutebBotschafter\PositionCheck
 */
class SitemapController
{
    /** @var */
    private $ignore;

    /**
     * RobotsController constructor.
     * @param $ignore
     */
    public function __construct($ignore)
    {
        $this->ignore = $ignore;
    }

    /**
     * @param $pages
     * @return string
     */
    private function buildXml($pages): string
    {
        $xml = "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">";

        foreach ($pages as $page) {
            if (in_array($page->uri(), $this->ignore) || $page->status() != "listed") {
                continue;
            }

            $xml .= "<url><loc>" .
                $page->url() .
                "</loc><lastmod>" .
                $page->modified("c") .
                "</lastmod><priority>" .
                ($page->isHomePage() ? 1 : number_format(0.5 / $page->depth(), 1)) .
                "</priority></url>";
        }

        $xml .= "</urlset>";

        return $xml;
    }

    /**
     * Shows the sitemap
     *
     * @return Response
     */
    public function show() : Response
    {
        $pages = site()->pages()->index();

        return new Response($this->buildXml($pages), "application/xml");
    }
}
