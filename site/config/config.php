<?php

return [
    'debug'  => true,
    "token" => "1b7afb63b2b4b3b7e12a6eb941b90be1f4b76fc8",
    "environments" => [
        "production" => "https://samuel-koch-und-freunde.de",
        "stage" => "https://skv.projektstatus.de",
        "virt" => "http://localhost"
    ]
];
