<aside class="ce-testimonial" id="<?= $data->slug() ?>">
  <div class="ce-testimonial__image" style="background-image: url('<?= $data->picture()->toFile()->url() ?>')"></div>
  <div class="ce-testimonial__text">
    <blockquote>
      <?= $data->caption()->kt() ?>
      <cite><?= $data->cite()->kti() ?></cite>
    </blockquote>
  </div>
</aside>
