<aside class="ce-image" id="<?= $data->slug() ?>">
  <div class="ce-image__image" style="background-image: url('<?= $data->picture()->toFile()->url() ?>')"></div>
  <div class="ce-image__text">
    <blockquote>
      <?= $data->caption()->kt() ?>
      <cite><?= $data->cite()->kti() ?></cite>
    </blockquote>
  </div>
</aside>
