<aside class="ce-textimage" id="<?= $data->slug() ?>">
  <div class="ce-textimage__image" style="background-image: url('<?= $data->picture()->toFile()->url() ?>')"></div>
  <div class="ce-textimage__text">
    <blockquote>
      <?= $data->caption()->kt() ?>
    </blockquote>
  </div>
</aside>
