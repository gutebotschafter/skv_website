<section class="ce-text ce-text--<?=$data->backgroundStyle()->html() ?>" id="<?= $data->slug() ?>">
    <?php if($data->picture()->isNotEmpty()): ?>
        <div class="ce-text__icon">
            <?php snippet('svg/mask') ?>
            <img src="<?= $data->picture()->toFile()->thumb(['width' => 240,'height'  => 240, 'crop' => true, 'quality' => 95])->url() ?>" alt="<?= $data->picture()->toFile()->title() ?>">
        </div>
    <?php endif ?>
  <div class="ce-text__text">
    <h2><?= $data->headline()->html() ?></h2>
    <?= $data->text()->kt() ?>
  </div>
</section>
