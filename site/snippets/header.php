<!DOCTYPE html>

<html lang="de">

  <head>
    <meta charset="utf-8">
    <meta name="robots" content="index, follow">
    <meta name="viewport" content="width=device-width, minimum-scale=1, initial-scale=1, shrink-to-fit=no">

    <title>
      <?php if($page->isHomePage()): ?>
        <?= $site->title()->html() ?><?php if($site->claim()->isNotEmpty()): ?> – <?= $site->claim()->html() ?><?php endif ?>
      <?php else: ?>
        <?= $page->title()->html() ?>  – <?= $site->title()->html() ?>
      <?php endif ?>
    </title>

    <meta name="author" content="<?= $site->author()->html() ?>">
    <meta name="description" content="<?= $site->description()->html() ?>">

    <link rel="shortcut icon" href="<?= url('assets/images/favicon.png') ?>">
    <link rel="apple-touch-icon-precomposed" href="<?= url('assets/images/appicon.png') ?>">

    <meta property="og:image" content="<?= url('assets/images/socialicon.png') ?>">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1024">
    <meta property="og:image:height" content="1024">

    <?= inlineRenderer('/assets/criticals.bundle.css', 'style') ?>
    <link rel="stylesheet" href="/assets/app.bundle.css">

    <?= $site->scriptsHead()->html() ?>

  </head>

  <body class="page page--<?= $page->template() ?>" id="<?= $page->slug() ?>">

    <?php snippet('page/page-header') ?>

    <?php snippet('page/page-menu') ?>

    <?php snippet('page/page-cta') ?>

    <main class="page-main">
