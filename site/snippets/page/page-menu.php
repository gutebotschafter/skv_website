<?php if($page->isHomepage()): ?>
  <nav class="page-menu">
    <?php foreach($site->index()->filterBy('template', 'default')->listed() as $item): ?>
      <a href="<?= $item->url() ?>" title="Die Seite <?= $item->title() ?> anzeigen"><?= $item->title() ?></a>
    <?php endforeach ?>
  </nav>
<?php else: ?>
  <nav class="page-menu">
    <a href="<?= $site->url() ?>" title="Zurück zur Startseite">&larr; Startseite</a>
  </nav>
<?php endif ?>
