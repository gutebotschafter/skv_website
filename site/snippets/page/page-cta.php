<?php if($page->showDonationCta() == 'true'): ?>
  <a class="page-cta<?php e($site->newsletterScript()->isNotEmpty(), ' page-cta--offset') ?>" id="page-cta" href="#page-footer" title="Informationen zum Spenden anzeigen" data-scroll>
    <span class="page-cta__text">
        jetzt
        <br><strong>spenden</strong>
    </span>
  </a>
<?php endif ?>
