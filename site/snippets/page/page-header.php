<header class="page-header">
  <?php if($page->isHomepage()): ?>
    <h1 class="page-header__logo"><?php snippet('svg/logo') ?></h1>
  <?php else: ?>
    <a class="page-header__logo" href="<?= $site->url() ?>" title="Zur <?= $site->title()->html() ?> Startseite">
      <?php snippet('svg/logo') ?>
    </a>
  <?php endif ?>
</header>
