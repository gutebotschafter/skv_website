<footer class="page-footer" id="page-footer">

    <?php if($site->donationCode()->toFile()): ?>
      <div class="page-footer__code">
        <img src="<?= $site->donationCode()->toFile()->url() ?>" alt="QR Code">
        <p>Scannen Sie diesen Code mit Ihrer Banking-App um zu spenden.</p>
      </div>
    <?php endif ?>

    <div class="page-footer__donate">
        <p>
            <strong>Spendenkonto:</strong>
            <br><span class="page-footer__donate__account"><?= $site->donationAccount()->kti() ?></span>
        </p>
        <p>
            <?= $site->donationNote()->kti() ?>
        </p>
        <p>
          <strong>Spenden über Paypal:</strong><br>
          <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
          <input type="hidden" name="cmd" value="_s-xclick" />
          <input type="hidden" name="hosted_button_id" value="Q8NK24SDLJPJW" />
          <input type="image" src="https://www.paypalobjects.com/de_DE/DE/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Spenden mit dem PayPal-Button" />
          <img alt="" border="0" src="https://www.paypal.com/de_DE/i/scr/pixel.gif" width="1" height="1" />
          </form>
        </p>
    </div>

    <div class="page-footer__contact">
        <p>
            <strong><?= $site->author()->html() ?></strong>
            <br><?= $site->address()->kti() ?>
        </p>
        <p>
            <strong>Nehmen Sie gerne mit uns Kontakt auf:</strong><br>
            <?php if($site->phone()->isNotEmpty()): ?>
                <a href="tel:<?= $site->phone()->html() ?>"><?= $site->phone()->html() ?></a><br>
            <?php endif ?>
            <a href="mailto:<?= $site->email()->html() ?>"
               title="<?= $site->author()->html() ?> eine E-Mail schreiben"><?= $site->email()->html() ?></a>
        </p>
    </div>

    <?php if($site->website()->isNotEmpty()): ?>
        <nav class="page-footer__website-link">
            <a href="<?= $site->website() ?>" title="Diese Webseite besuchen">
                <span><?php e($site->WebsiteTitle()->isNotEmpty(),  'Die Webseite ' . $site->WebsiteTitle()->html() . ' besuchen!', 'Zur Webseite') ?></span>
            </a>
        </nav>
    <?php endif ?>

    <nav class="page-footer__meta">
        <?php foreach ($site->index()->filterBy('template', 'meta') as $item): ?>
            <a href="<?= $item->url() ?>" title="Die Seite <?= $item->title() ?> anzeigen"><?= $item->title() ?></a>
        <?php endforeach ?>
    </nav>

</footer>
