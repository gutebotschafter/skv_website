<?php if($site->newsletterScript()->isNotEmpty()): ?>
  <aside class="page-newsletter" id="page-newsletter">
      <div class="page-newsletter__title trigger-newsletter">
          <span class="page-newsletter__title__text">Newsletter abbonieren</span>
          <?php snippet('svg/icons/x') ?>
      </div>
      <div class="page-newsletter__form">
          <?= $site->newsletterScript() ?>
      </div>
  </aside>
<?php endif ?>
