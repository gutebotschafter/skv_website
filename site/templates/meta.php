<?php snippet('header') ?>

<header class="title">
  <div class="title__text">
    <h1><?= $page->title()->html() ?></h1>
  </div>
</header>

<div class="ce-text">
  <div class="ce-text__text">
    <?= $page->text()->kirbytext() ?>
  </div>
</div>

<?php snippet('footer') ?>
