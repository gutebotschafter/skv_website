<?php snippet('header') ?>

  <header class="title">
    <div class="title__text">
      <?= $page->intro()->kirbytext() ?>
    </div>
  </header>

  <aside class="cover">
    <div class="cover__claim">
      <?php snippet('svg/claim') ?>
    </div>
    <div class="cover__image" style="background-image: url('<?= $page->cover()->toFile()->url() ?>')"></div>
  </aside>

  <?php foreach ($page->children()->listed() as $item): ?>

    <?php snippet('ce/' . $item->template(), ['data' => $item]) ?>

  <?php endforeach ?>

<?php snippet('footer') ?>
