<?php

if (version_compare(phpversion(), '7.1.0', '>=') === false) {
    die(include __DIR__ . '/kirby/views/php.php');
}

$autoloader = __DIR__ . DIRECTORY_SEPARATOR . 'vendor/autoload.php';
if (file_exists($autoloader)) {
    include($autoloader);
}

$root = __DIR__ . DIRECTORY_SEPARATOR . 'public';

$kirby = new Kirby([
    'roots' => [
      'index'   => $root,
      'site'    => __DIR__ . DIRECTORY_SEPARATOR . 'site',
      'content' => __DIR__ . DIRECTORY_SEPARATOR . 'content',
      'media'   => $root . DIRECTORY_SEPARATOR . 'media'
    ]
  ]);

echo $kirby->render();
