import './polyfills';

import '../assets/images/favicon.png';
import '../assets/images/appicon.png';
import '../assets/images/socialicon.png';
import '../assets/images/sun.svg';

import * as scrollinspector from './components/scrollinspector';
import * as newsletter from './components/newsletter';
import * as contentScroll from './components/content-scroll';

scrollinspector.init();
newsletter.init();
contentScroll.init();
