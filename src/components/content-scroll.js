import SmoothScroll from '../vendor/smooth-scroll.min';

export const init = () => {
  // smooth-scroll
  const scroll = new SmoothScroll('a[href*="#"]', {
    offset: 0
  } );

};
