<?php

/**
 * Class Updater
 */
class Updater
{
    /**
     * @var string
     */
    private $tempDirectory = __DIR__ . "/temp";

    /**
     * @var bool
     */
    private $isUpdateReady = false;

    /**
     * @var string
     */
    private $host = "";

    /**
     * @var string
     */
    private $token = "";

    /**
     * @var string
     */
    private $route = "/gb/updater";

    /**
     * @var string
     */
    private $content = "";

    /**
     * @var string
     */
    private $media = __DIR__ . "/public/media";

    /**
     * Updater constructor.
     *
     * @param $host
     * @param $token
     * @param $content
     */
    public function __construct($host, $token, $content = "content")
    {
        ini_set("allow_url_fopen", 1);

        $this->host = $host;
        $this->token = $token;
        $this->content = __DIR__ . "/" . $content;

        $this->checkTempDirectory();
    }

    /**
     * Checks the temporary directory
     */
    private function checkTempDirectory()
    {
        if (is_dir($this->tempDirectory)) {
            $this->rec_rmdir($this->tempDirectory);
        }

        mkdir($this->tempDirectory, 0777, true);

        if (is_dir($this->tempDirectory) && is_writable($this->tempDirectory)) {
            $this->isUpdateReady = true;
        }
    }

    /**
     * @param $path
     * @return int
     */
    private function rec_rmdir($path) : int
    {
        if (!is_dir($path)) {
            return -1;
        }

        $dir = @opendir($path);

        if (!$dir) {
            return -2;
        }

        while (($entry = @readdir($dir)) !== false) {
            if ($entry == '.' || $entry == '..') {
                continue;
            }

            if (is_dir($path . '/' . $entry)) {
                $res = $this->rec_rmdir($path . '/' . $entry);
                if ($res == -1) {
                    @closedir($dir);
                    return -2;
                } elseif ($res == -2) {
                    @closedir($dir);
                    return -2;
                } elseif ($res == -3) {
                    @closedir($dir);
                    return -3;
                } elseif ($res != 0) {
                    @closedir($dir);
                    return -2;
                }
            } elseif (is_file($path . '/' . $entry) || is_link($path . '/' . $entry)) {
                $res = @unlink($path . '/' . $entry);

                if (!$res) {
                    @closedir($dir);
                    return -2;
                }
            } else {
                @closedir($dir);
                return -3;
            }
        }

        @closedir($dir);

        $res = @rmdir($path);

        if (!$res) {
            return -2;
        }

        return 0;
    }


    /**
     * Checks for updates
     */
    public function update()
    {
        $tempUpdateFile = $this->tempDirectory . "/temp.zip";

        if (is_writable($this->tempDirectory)) {
            echo "check for update...\n";

            $ch = curl_init();
            $source = $this->host . $this->route . "/?token=" . $this->token;
            curl_setopt($ch, CURLOPT_URL, $source);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $data = curl_exec($ch);
            curl_close($ch);

            file_put_contents($tempUpdateFile, $data);

            $zip = new ZipArchive;
            $res = $zip->open($tempUpdateFile);

            if ($res === true) {
                echo "update found...\n";

                $zip->extractTo($this->tempDirectory);
                $zip->close();

                $this->rec_rmdir($this->content);

                unlink($tempUpdateFile);

                rename($this->tempDirectory, $this->content);

                echo "content updated ..\n";
                echo "deleting media folder...\n";

                $this->rec_rmdir($this->media);

                echo "all done\n";
            } else {
                echo "could not get content from " . $this->host;
            }
        }
    }
}

$config = include(__DIR__ . "/site/config/config.php");
$environment = "";

if (isset($argv) && count($argv) > 1 && array_key_exists($argv[1], $config["environments"])) {
    $environment = $config["environments"][$argv[1]];
} elseif (isset($_GET["env"]) && array_key_exists($_GET["env"], $config["environments"])) {
    $environment = $config["environments"][$_GET["env"]];
} else {
    die("Missing parameter environment. Accept: " . implode(", ", array_keys($config["environments"])));
}

$updater = new Updater($environment, $config["token"], "content");
$updater->update();
